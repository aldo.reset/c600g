# C600G

Accurate Apple IIe Emulator

(C)opyleft Aldo Reset from CCB (https://cleancrackband.wordpress.com)

---------------------------------------------------------------------------------
Use Ebiten: https://github.com/hajimehoshi/ebiten 
Ebiten is an open source game library for the Go programming language. Ebiten's simple API allows you to quickly and easily develop 2D games that can be deployed across multiple platforms.



TODO LIST MAKE EMULATOR:
- CPU : 6502A cycle accurate , Apple IIe non enhanced (65C02 is ENHANCED)
- MMU : Bus and memory with Roms (64k)
- IOU : Soft switch
- Slots : 
	- 64k extension, 80c with Chatmauve EVE RGB (Rev B)
	- Disk II (Woz format only R/W) + 2 x FLOPPY
	- SOUND : PHASOR
	
---------------------------------------------------------------------------------

To Start Dev (ie: Gnu/Linux Debian):
sudo apt install golang-go golint git     (Golang >= 1.15)
sudo apt install libc6-dev libglu1-mesa-dev libgl1-mesa-dev libxcursor-dev libxi-dev libxinerama-dev libxrandr-dev libxxf86vm-dev libasound2-dev pkg-config

Install your favorite IDE (Geany...): 
ie: apt install geany

Initialize the project on your system :
git clone https://gitlab.com/aldo.reset/c600g/

cd c600g

To finish you can build project:

go build

