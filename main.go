/*Package main provide Ebiten environment for many devices
  main.go

    (C)opyleft 2021 Aldo Reset from CCB (https://cleancrackband.org)


*/
package main

import (
    "github.com/hajimehoshi/ebiten/v2"
    "c600g/Motherboard/Cpu"
	"time"
	"fmt"
//	"log"
)

var computerCpu cpu.M6502a
const tps = 60


func check(e error) {
	if e != nil {
		panic(e)
	}
}

type emulator struct {
	screen       	*ebiten.Image
	screenWidth  	int
	screenHeight 	int
	tps				int

	//isRunning  bool
	// emulate    computer.Computer
}

func (e *emulator) Update() error {

	return nil

}

func (e *emulator) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {

	return e.screenWidth, e.screenHeight
}

func (e *emulator) Draw(screen *ebiten.Image) {
	op := &ebiten.DrawImageOptions{}

	//e.screen = computer.UpdateComputer()

	screen.DrawImage(e.screen, op)

}

func main() {
	fmt.Println("Go ! ")
	ebiten.SetWindowSize(1024, 768)
	ebiten.SetWindowTitle("C600G - Accurate Apple IIe Emulator")
	ebiten.SetMaxTPS(tps)
	frequency := computerCpu.M6502aInit(false)
	// Start CPU with frequency
	go func() {
	for  range time.Tick(frequency * time.Nanosecond) {
		computerCpu.CpuStep()
	}
	}()


}

