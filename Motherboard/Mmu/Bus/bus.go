/*Package bus provides a mappable 16-bit addressable 8-bit data bus
  Different Memory backends can be attached at different base addresses.

   bus.go

   (C)opyleft 2021 Aldo Reset from CCB (https://cleancrackband.org)


*/
package bus

import (
	"c600g/Motherboard/Mmu/Memory"
	"fmt"
	"io/ioutil"
)

type busEntry struct {
	mem          *memory.Mem
	startAddress uint16
	endAddress   uint16
	softswStatus uint16
}

type busDesc struct {
	hasEve    bool
	hasPhasor bool
}

const SOFTSW_KSTROBE = uint16(1 << 0)   // $C010
const SOFTSW_RDBNK2 = uint16(1 << 1)    // $C011
const SOFTSW_RDLCRAM = uint16(1 << 2)   // $C012
const SOFTSW_RAMRD = uint16(1 << 3)     // $C013
const SOFTSW_RAMWRT = uint16(1 << 4)    // $C014
const SOFTSW_SLOTCXROM = uint16(1 << 5) // $C015
const SOFTSW_RDALTZP = uint16(1 << 6)   // $C016
const SOFTSW_SLOTC3ROM = uint16(1 << 7) // $C017
const SOFTSW_80STORE = uint16(1 << 8)   // $C018
const SOFTSW_VBL = uint16(1 << 9)       // $C019
const SOFTSW_TEXT = uint16(1 << 10)     // $C01A
const SOFTSW_MIXED = uint16(1 << 11)    // $C01B
const SOFTSW_PAGE2 = uint16(1 << 12)    // $C01C
const SOFTSW_HIRES = uint16(1 << 13)    // $C01D
const SOFTSW_ALTCHAR = uint16(1 << 14)  // $C01E
const SOFTSW_80VID = uint16(1 << 15)    // $C01F



// Bus is a 16-bit address, 8-bit data bus, which maps reads and writes
// at different locations to different backend Memory. For example the
// lower 32K could be RAM, the upper 8KB ROM, and some I/O in the middle.
type Bus struct {
	entries []busEntry
}

// NewBus create a new bus for computer where access memory and io
func NewBus() (Bus, error) {
	fmt.Println("StartBus")
	var err error
	computerbus := Bus{entries: make([]busEntry, 0)}
	mainRAM, _ := memory.NewMem("mainRam", 0xBE00) // 48k
	auxRAM, _ := memory.NewMem("auxRam", 0xBD00)
	mainZPG, _ := memory.NewMem("mainZPG", 0x0200) // 48k
	auxZPG, _ := memory.NewMem("auxZPG", 0x0200)
	mainBankRAM, _ := memory.NewMem("mainBankRAM", 0x2000)   // 8k
	mainbank0RAM, _ := memory.NewMem("mainbank0RAM", 0x1000) // 4k
	mainbank1RAM, _ := memory.NewMem("mainbank1RAM", 0x1000)
	auxBankRAM, _ := memory.NewMem("auxBankRAM", 0x2000)   // 8k
	auxbank0RAM, _ := memory.NewMem("auxbank0RAM", 0x1000) // 4k
	auxbank1RAM, _ := memory.NewMem("auxbank1RAM", 0x1000)

	C000m := memfromFile("./Roms/CD342-0135-B.bin", "IoBank", 0, 256) // everything is off (an2 & an3 on (0x80) ???)

	//SLOTm := memfromFile("./C600G/computers/Slots/", "Slots")
	//SLOT3m := memfromFile("./C600G/computers/Slots/Aux/SLOT3AUX.bin", "SLOT3AUX", 0, 256) // SLOT3

	
	C100CFFF := memfromFile("./Roms/CD342-0135-B.bin", "romCD", 256, 3840)
	D000DFFF := memfromFile("./Roms/CD342-0135-B.bin", "romCD", 4096, 4096)

	E000FFFF := memfromFile("./Roms/EF342-0134-A.bin", "romEF", 0, 8192) // EF342-0134-A.bin

	// Attach(mem memoire.Memory, startAddress uint6, active bool  // order is important (more big to more small, ram befor rom)  for same address active last one is taken. hi > 7 = RAM: lo > 7 = AUX
	computerbus.Attach(mainRAM, 0x0200, 0x0000)
	computerbus.Attach(auxRAM, 0x0200, SOFTSW_RAMRD|SOFTSW_RAMWRT)
	computerbus.Attach(mainZPG, 0x0000, 0x0000)
	computerbus.Attach(auxZPG, 0x0000, SOFTSW_RDALTZP)
	computerbus.Attach(C000m, 0xC000, 0x0000)
	computerbus.Attach(C100CFFF, 0xC100, 0x0000)
	//computerbus.Attach(SLOT3m, 0xC300, SOFTSW_SLOTC3ROM) // only select if RomisActive becase if not slot is prefered.
	//computerbus.Attach(SLOTm, 0xC100, SOFTSW_SLOTCXROM)
	computerbus.Attach(mainbank1RAM, 0xD000, SOFTSW_RDBNK2) // when reset bank 2 use (bank0)
	computerbus.Attach(mainbank0RAM, 0xD000, 0x0000)        // when reset bank 2 use (bank0)
	computerbus.Attach(mainBankRAM, 0xE000, 0x0000)
	computerbus.Attach(auxbank1RAM, 0xD000, SOFTSW_RDBNK2|SOFTSW_RDLCRAM) // when reset bank 2 use (bank0)
	computerbus.Attach(auxbank0RAM, 0xD000, SOFTSW_RDLCRAM)               // when reset bank 2 use (bank0)
	computerbus.Attach(auxBankRAM, 0xE000, SOFTSW_RDLCRAM)
	computerbus.Attach(D000DFFF, 0xD000, 0x0000)
	computerbus.Attach(E000FFFF, 0xE000, 0x0000)
	// read data for test
	//computerbus.datafromFile("./text.bin", 0x400)
	// check(err)

	return computerbus, err
}

// Shutdown tells the address bus a shutdown is occurring, and to pass the
// message on to subordinates.
func (b *Bus) Shutdown() {
	for _, backEnd := range b.entries {
		backEnd.mem.Shutdown()
	}
}

func (b *Bus) String() string {
	return fmt.Sprintf("Address bus ", b)
}

// Attach maps a bus address range to a backend Memory implementation,
// which could be RAM, ROM, I/O device etc.
func (b *Bus) Attach(mem *memory.Mem, startaddress uint16, memstatus uint16) error {
	endaddress := startaddress + uint16(mem.Size) - 1
	entry := busEntry{mem: mem, startAddress: startaddress, endAddress: endaddress, softswStatus: memstatus}
	// fmt.Println("Entry: ", entry)
	b.entries = append(b.entries, entry)

	return nil
}

func (b *Bus) backendParse(address uint16) (*memory.Mem, uint16, error) {
	var memoireAccess memory.Mem
	var err error
	var offSet uint16

	err = fmt.Errorf("No read backend for address 0x%04X", address)

	for _, backEnd := range b.entries {
		//fmt.Println(backEnd.readStatus, backEnd.startAddress, backEnd.endAddress)
		if address >= backEnd.startAddress && address <= backEnd.endAddress {
			memoireAccess = *backEnd.mem
			offSet = address - backEnd.startAddress
			//memStatus = backEnd.softswStatus

			err = nil
		}

	}
	// fmt.Print(memoireAccess," : ", offSet)
	return &memoireAccess, offSet, err
}

// ReadByte returns the byte from memory mapped to the given address.
// e.g. if ROM is mapped to 0xC000, then Read(0xC0FF) returns the byte at
// 0x00FF in that RAM device.
func (b *Bus) readMemory(address uint16) byte {
	mem, offSet, err := b.backendParse(address)
	check(err)
	// 1
	//fmt.Printf("offSet : 0x%04X\n", offSet)
	// value := iou.IoRead(offSet)
	// fmt.Print("Mémoire: ", mem)
	value := mem.ReadByte(offSet)

	return value
}

func (b *Bus) Read(address uint16) byte {
	value := b.readMemory(address)
	
	return value
}



// Read16 returns the 16-bit value stored in little-endian format with the
// low byte at address, and the high byte at address+1.
func (b *Bus) Read16(address uint16) uint16 {
	lo := uint16(b.readMemory(address))
	hi := uint16(b.readMemory(address + 1))
	return hi<<8 | lo
}

// WriteByte the byte to the device mapped to the given address.
func (b *Bus) writeMemory(address uint16, value byte) {
	mem, offSet, err := b.backendParse(address)
	if err == nil { // Apple do not break when nothing write address only if nothing read address

		mem.WriteByte(offSet, value)
	}

}

func (b *Bus) Write(address uint16, value byte) {
		b.writeMemory(address, value)
}



// Write16 writes the given 16-bit value to the specifie address, storing it
// little-endian, with high byte at address+1.
func (b *Bus) Write16(address uint16, value uint16) {
	b.writeMemory(address, byte(value))
	b.writeMemory(address+1, byte(value>>8))
}

// datafromFile Load binary/hex File to Bus/memory (usely ROM)
func memfromFile(path string, name string, offSet int, longueur int) *memory.Mem {
	data, err := ioutil.ReadFile(path)
	check(err)
	if longueur == -1 {
		longueur = len(data)
	}
	lastByte := offSet + longueur

	return &memory.Mem{Name: name, Size: longueur, Data: data[offSet:lastByte]}

}

// for test purpose
func (b *Bus) datafromFile(path string, startAddress uint16) {
	data, err := ioutil.ReadFile(path)
	check(err)
	compteur := 0
	for compteur < len(data) {
		b.writeMemory(startAddress+uint16(compteur), data[compteur])
		compteur++
	}

}

func MemfromPath(path string, name string) *memory.Mem {
	var datas, data []byte
	var nameFile string
	files, err := ioutil.ReadDir(path)
	check(err)

	for _, file := range files {
		if file.IsDir() == false {
			nameFile = file.Name()
			data, err = ioutil.ReadFile(path + nameFile)
			check(err)
			datas = append(datas, data[:]...)
		}

	}
	longueur := len(datas)

	return &memory.Mem{Name: name, Size: longueur, Data: datas}

}

func DatafromFile(path string, offSet int, longueur int) ([]byte, error) {
	data, err := ioutil.ReadFile(path)
	check(err)
	if longueur == -1 {
		longueur = len(data)
	}
	lastByte := offSet + longueur

	return data[offSet:lastByte], nil

}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
