/*Package mmu provides an equivalent Apple IIe MemoryManagerUnit

  mmu3440010b.go

   (C)opyleft 2021 Aldo Reset from CCB (https://cleancrackband.org)


*/
package mmu

import (
	"Motherboard/Mmu/Bus"
)

var ComputerBus *bus.Bus

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func InitMemory() {
	var err error
	ComputerBus, err = bus.NewBus()
	check(err)
}

func Read(address uint16) uint8 {

	return ComputerBus.ReadMemory(address)

}

func Write(address uint16, value uint8) {

	ComputerBus.WriteMemory(address, value)

}
